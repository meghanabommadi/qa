class Graph:
	#Undirected Graph
	DEFAULT_EDGE_WEIGHT = 0
	def __init__(self) : 
		self.adj_list = {} #Neighbours of each node

	def length(self):
		return len(self.adj_list)

	def has_node(self,node): 
		return node in self.adj_list
	
	def has_edge(self,edge):
		u,v = edge
		return (u in self.adj_list
			and v in self.adj_list
			and v in self.adj_list[u]
			and u in self.adj_list[v])	
	
	def edge_weight(self, edge):
		u,v = edge
		return self.adj_list.get(u, {}).get(v, self.DEFAULT_EDGE_WEIGHT)	
	def add_node(self,item):
		if item in self.adj_list:
			return None
		self.adj_list[item] = {}
		return item

	def add_edge(self,edge,wt=1.0):
		if wt == 0.0: # empty edge is equivalent to no edge at all
			if self.has_edge(edge):
				self.del_edge(edge)  
			return
		u,v = edge
		if v not in self.adj_list[u] or u not in self.adj_list[v]:
			self.adj_list[u][v] = wt
			self.adj_list[v][u] = wt
			return 0
		return None
	def del_edge(self,edge):
		u,v = edge
		del self.adj_list[u][v]
		if u!= v:
			del self.adj_list[v][u]
	def del_node(self,node):
		for n in self.adj_list[node].keys():
			if n != node:
				self.del_edge((n,node))

		del self.adj_list[node]

	def nodes(self):
		return list(self.adj_list)

	def edges(self):
		result = []
		for u in self.adj_list:
			for v in self.adj_list[u]:
				result.append((u,v))
		return result		

