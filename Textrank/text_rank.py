import re
from six import iteritems
from gensim.summarization.bm25 import get_bm25_weights
from gensim.summarization.summarizer import summarize_corpus
from textrank_utils import Graph
from preprocessing import clean_text
import numpy
from scipy.linalg import eig
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import eigs
import math

def tagfile_preprocess(tag_file, input_sents, original):
	tag_sentences = [] #Array of dictionaries
	f = open(tag_file,'r')
	contents = f.read()
	f.close()
	sents = contents.split('</s>\n<s>\n')
	sents = sents[:-1] #Since it ends with the delim

	#Assert len(sents) == len(input_sents)

	for i in range(len(sents)):
		this_sent = sents[i].split('\n')
		this_input_sent = input_sents[i].split(' ')
		index = 0
		##print(original[i])
		this_tags = []
		for w in this_sent:
			if w == '' :
				continue
			tags = w.split('\t')
			word, lemma1, POS1, suff1, coarse1,dum, gen1, num1,_ = tags
			if index< len(this_input_sent) and word == this_input_sent[index]	:
				
				this_tags.append([word, lemma1, POS1, suff1, coarse1, gen1, num1])
				index  = index + 1
		tag_sentences.append(this_tags)
	return tag_sentences

def build_corpus(filtered_sentences):
	split_tokens = [ sent.split() for sent in filtered_sentences]

	token2id = {}
	corpus = []
	#change from document to bag of words ; token id, token_count
	for token in split_tokens:
		#Add document to dictionary	
		frequency = {}
		for w in token:
			if w in frequency:
				frequency[w] = frequency[w]+1
			else:
				frequency[w] = 1
		missing = sorted(x for x in frequency.keys() if x not in token2id)
		for w in missing:
			token2id[w] = len(token2id)
		
		result = {token2id[w]:frequency[w] for w in frequency.keys() if w in token2id}
		result = sorted(iteritems(result))
		corpus.append(result)
	return corpus
def set_graph_edge_weights(graph, method=2):
	nodes = graph.nodes()
	#1. Paper Sentence similarity 
	# 1.0 Complete set = for now
	# 1.1 Stemming = should have done 1.1 and 1.2 in preprocessing
	# 1.2 POS Tagging
	if method == 1: #Paper sentence_similarity
		for i in range(len(nodes)):
			for j in range(len(nodes)):
				Si = [n[0] for n in nodes[i]] #words
				Sj = [n[0] for n in nodes[j]]#words
				num_common_words = len(set(Si).intersection(set(Sj)))
				if i == j or num_common_words < 0.001:
					continue
				similarity = num_common_words  #btw 2 Si and Si
				denom = math.log(len(Si))+math.log(len(Sj))
				similarity = similarity/denom
				if graph.has_edge((nodes[i],nodes[j])):
					graph.del_edge((nodes[i],nodes[j]))
				graph.add_edge((nodes[i],nodes[j]),similarity)
						
	elif method == 2: #Best Match weights
		#print("Here")
		weights = get_bm25_weights(nodes)	
		for i, doc_bow in enumerate(weights):
			for j, weight in enumerate(doc_bow):
				if i == j or weight < 0.001:
					continue
				if not graph.has_edge((nodes[i],nodes[j])):
					graph.add_edge((nodes[i],nodes[j]),weight)	
	
	#If nothing is similar
	if all(graph.edge_weight(edge)==0 for edge in graph.edges()):
		for i in range(len(nodes)):
			for j in range(len(nodes)):
				if graph.has_edge((nodes[i],nodes[j])):
					graph.del_edge((nodes[i],nodes[j]))
				graph.add_edge((nodes[i],nodes[j]),1)

def pagerank_weighted(graph, damping=0.85):
	coeff_adjacency_matrix = build_adjacency_matrix(graph, coeff=damping)
	probabilities = (1 - damping) / float(graph.length())

	pagerank_matrix = coeff_adjacency_matrix.toarray()
	pagerank_matrix += probabilities
	
	a = pagerank_matrix.T
	vals, vecs = eig(a)
	ind = numpy.abs(vals).argmax()
	vec = vecs[:, ind].real
	if len(a) >=3:
		vals, vecs = eigs(a, k=1)
		vec = vecs[:, 0].real

	scores = {}
	for i, node in enumerate(graph.nodes()):
		scores[node] = abs(vec[i])

	return scores

def build_adjacency_matrix(graph, coeff=1):
	row = []
	col = []
	data = []
	nodes = graph.nodes()
	nodes2id = {v: i for i, v in enumerate(nodes)}
	length = len(nodes)

	for i in range(length):
		current_node = nodes[i]
		neighbors = list(graph.adj_list[current_node])
		neighbors_sum = sum(graph.edge_weight((current_node, neighbor)) for neighbor in neighbors)
		for neighbor in neighbors:
			edge_weight = float(graph.edge_weight((current_node, neighbor)))
			if edge_weight != 0.0:
				row.append(i)
				col.append(nodes2id[neighbor])
				data.append(coeff * edge_weight / neighbors_sum)

	return csr_matrix((data, (row, col)), shape=(length, length))

def textrank_summarize_corpus(corpus,sentences,ratio=0.7):
		
	hashable_corpus = []
	#Build graph 
	graph = Graph()
	for doc in corpus:
		hash_doc = tuple(doc)
		hashable_corpus.append(hash_doc)
		if not graph.has_node(hash_doc):
			graph.add_node(hash_doc)
	sentences_by_corpus = dict(zip(hashable_corpus, range(len(sentences))))
	#print(sentences_by_corpus[hashable_corpus[13]])
	#set graph edge weights
	set_graph_edge_weights(graph)
		
	#remove unreachable nodes
	for node in graph.nodes():
		if all(graph.edge_weight((node, other)) == 0 for other in list(graph.adj_list[node])):
			graph.del_node(node)
	
	#get page rank scores of that graph
	scores = pagerank_weighted(graph)	
	hashable_corpus.sort(key=lambda doc: scores.get(doc, 0), reverse=True)	
	top_n_bow = [list(doc) for doc in hashable_corpus[:int(len(corpus)*ratio)]]
	#inbuilt =  summarize_corpus(corpus,ratio=ratio)
	indices =  [sentences_by_corpus[tuple(doc)] for doc in top_n_bow]
	indices.sort()
	return [sentences[i] for i in indices]
if __name__ == '__main__':

	tag_file = 'story_tags.txt'
	filename = 'sample_story.txt'
	f = open(filename,'r')
	contents = f.read()
	
	#Preprocess the text first
	sentences, filtered_sentences = clean_text(contents)	

	tag_sentences = tagfile_preprocess(tag_file,filtered_sentences, sentences)
	lemma_sentences = []
	for i in range(len(tag_sentences)):
		this_tag = tag_sentences[i]
		this_sent= []
		for j in range(len(this_tag)):
			this_sent.append(this_tag[j][1])
		lemma_sentences.append(this_sent)

	#Build corpus
	corpus = build_corpus(filtered_sentences)	

	#Summarize corpus
	summary = textrank_summarize_corpus(corpus,sentences)	
	
	#Sequence of the sentences.
	
	#print(summary)
	for sentence in summary:
		print(sentence)
