0. All Different Stories are present in StoriesDataset Directory
	These were taken from kathalu.wordpress.com

1. For Summarization

	a.Text Rank Summarization
	b.Frequency Based Summarization

		i. To get the TextRank Summarization
		
			cd TeluguPOSTagger
			cp ../StoriesDataset/1.txt telugu.input.txt
			make tag
	
		    cd Textrank
			cp ../StoriesDataset/1.txt sample_story.txt	
			cp ../TeluguPOSTagger/telugu.output.txt story_tags.txt
			python text_rank.py > sample_output.txt
		
		Note that the default option for summarization is currently 0.7
		You can always change it in text_rank.py
        
        ii. To get the word frequency Summarization
            cd QuestionGeneration1
            mv ../StoriesDataset/1.txt telugu.input.txt
            make tag 
            awk {'print $2'} telugu.output.txt > rwords.txt
            python3 rootw.py > rootwords.txt
            python3 freq.py 1.txt rootwords.txt
            
            
            python3 freq.py 1.txt rootwords.txt > telugu.input.txt
            cat telugu.input.txt 

2. For POS Tagging
	Note that this module was not built by us, and was taken from 
		https://bitbucket.org/sivareddyg/telugu-part-of-speech-tagger.git. 

	For Details on how to run this, refer to the README file in the directory. 
	
	i. To get the POS Tags for our summary
		
		cd TeluguPOSTagger
		Copy the summarized file from part 1. to this directory
		Rename it to telugu.input.txt
		make tag		

3. For UD Tagging
	Note that we have built this model, and trained it using the Telugu UD TreeBank in the UniversalDependencies repository. 

	i. To train the model 

		cd TeluguPOSTagger
		python udtags.py kothistory.txt kothistory.txt #or any other story you can copy to the directory and use 
	
	This step will save the model and required files as
		saved_model.json (summary)
		saved_model.h5 (weights)
		saved_word2index.json
		saved_tag2index.json

	ii. To run the saved model	

		cd QuestionGeneration2
		python load_ud.py

	This step will print the UD tags and also save them in the file UD_tags.npy
		
		To load the tags, use numpy.load()

4. For Question Generation.
	Note that you have to copy story file from dataset to telugu.input.txt if you are running this in isolation.
    
    i. In QuestionGenerator1 directory:
		
        run the commands:
		make tag
        python3 questioncode.py 1.txt rwords.txt 

	ii. In QuestionGeneration2 Directory:
		
	    run the commands:

		cp ../TeluguPOSTagger/telugu.input.txt ./
	  	cp ../TeluguPOSTagger/telugu.output.txt ./

		python load_ud.py
		python q_form.py > outputS.txt

### TO DO:
1. Anaphora Resolution
2. Improve Dependency Parser 	
3. Expand Dataset/Domain
4. Add Performance Measure Metrics
