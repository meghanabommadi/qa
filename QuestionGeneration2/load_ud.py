from preprocessing import *
from keras.models import model_from_json
import json
from keras.preprocessing.sequence import pad_sequences
import numpy as np

def tokenize(bookl):
    if bookl is not None:
        words = bookl.replace(', ',' ')
        words = bookl.replace(',',' ')
        words = bookl.replace('. ',' ')
        words = bookl.replace('.', ' ' )
        words = bookl.replace('"', '" ' )
        words = bookl.replace('', '\s"\s' )
        # print(book)
        words = bookl.split()
        return words
    else:
        return None

MAX_LENGTH = 20

input_filename = './telugu.input.txt'
f = open(input_filename,'r')
input_contents = f.read()
#Input sentences has normal split sentences
#Input filtered sentences - same but without the punctuation
input_sentences, input_filtered_sentences = clean_text(input_contents)
f.close()

with open('saved_word2index.json') as fp:
	word2index = json.load(fp)

with open('saved_tag2index.json') as fp:
	tag2index = json.load(fp)

json_file = open('saved_model.json','r')
loaded_model_json = json_file.read()
json_file.close()

model = model_from_json(loaded_model_json)
model.load_weights('saved_model.h5')

tokens = []
for i in range(len(input_sentences)):
	tokens.append(tokenize(input_sentences[i]))
	tokens[i].append('.')

test_samples_X = []
for s in tokens:
    s_int = []
    for w in s:
        try:
            s_int.append(word2index[w.lower()])
        except KeyError:
            s_int.append(word2index['-OOV-'])
    test_samples_X.append(s_int)

test_samples_X = pad_sequences(test_samples_X, maxlen=MAX_LENGTH, padding='post')

predictions = model.predict(test_samples_X)
def logits_to_tokens(sequences, index):
    token_sequences = []
    for categorical_sequence in sequences:
        token_sequence = []
        for categorical in categorical_sequence:
            token_sequence.append(index[np.argmax(categorical)])

        token_sequences.append(token_sequence)

    return token_sequences

newtokens = logits_to_tokens(predictions, {i: t for t, i in tag2index.items()})
np.save('UD_tags.npy',newtokens)
print(newtokens)
print(input_sentences)

	
