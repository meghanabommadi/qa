import re

def preprocess(sentences):
	preprocessed= []
	punctuation = '!\"“#”$%&\'()*+,-./:;<=>?@[\]^_`{|}~'
	for sent in sentences:
		#s = str(sent.encode('utf8'))
		s = sent.lower()
		RE_PUNCT = re.compile(r'([%s])+' % re.escape(punctuation), re.UNICODE)
		RE_TAGS = re.compile(r"<([^>]+)>", re.UNICODE)	
		RE_WHITESPACE = re.compile(r"(\s)+", re.UNICODE)
		
		s=RE_TAGS.sub(" ",s)
		s=RE_PUNCT.sub(" ",s)
		s=RE_WHITESPACE.sub(" ",s)	
		#Can remove stop words
		#Can do stemming
		#Can remove articles?
		preprocessed.append(s.split())
			
	return preprocessed

def get_sentences(text):
	#text = " ".join([ t for t in text.split('\n') if t !='' ])
	RE_SENTENCE = re.compile(r'(\S.+?[.?])(?=\s+|$)|(\S.+?)(?=[\n]|$)', re.UNICODE)
	for match in RE_SENTENCE.finditer(text):
		yield match.group()

def clean_text(text):
	sentences = [s for s in get_sentences(text)]
	filtered_sentences = [" ".join(s) for s in preprocess(sentences)]
	return sentences, filtered_sentences

