from preprocessing import *
import numpy as np
# from load_ud import tokenize
def tokenize(bookl):
    if bookl is not None:
        words = bookl.replace(',',' ,')
        words = bookl.replace(',',' ,')
        words = bookl.replace('. ',' ')
        words = bookl.replace('.', ' ' )
        words = bookl.replace('"', '" ' )
        words = bookl.replace('', '\s"\s' )
        # print(book)
        words = bookl.split()
        return words
    else:
        return None

# from indicnlp.
def tagfile_preprocess(tag_file, input_sents, original):
	tag_sentences = [] #Array of dictionaries
	f = open(tag_file,'r')
	contents = f.read()
	f.close()
	sents = contents.split('</s>\n<s>\n')
	sents = sents[:-1] #Since it ends with the delim

	#Assert len(sents) == len(input_sents)

	for i in range(len(sents)):
		this_sent = sents[i].split('\n')
		this_input_sent = input_sents[i].split(' ')
		index = 0
		##print(original[i])
		this_tags = []
		for w in this_sent:
			if w == '' :
				continue
			tags = w.split('\t')
			word, lemma1, POS1, suff1, coarse1,dum, gen1, num1,_ = tags
			if index< len(this_input_sent) and word == this_input_sent[index]	:
				
				this_tags.append([word, lemma1, POS1, suff1, coarse1, gen1, num1])
				index  = index + 1
		tag_sentences.append(this_tags)
	return tag_sentences

def positive_questions(tag_sents, input_sents):
	#Assert both lengths are equal
	yes_questions = []
	yes_answers = []
	avunu = 'అవును'
	kadhu = 'కాదు'
	ledhu = 'లేదు'
	vowel_aa = b'\xe0\xb0\xbe'.decode('utf-8')
	for i in range(len(input_sents)):
		#Generate positive question iff sentence ends with VM.
		this_tag_sent = tag_sents[i]
		if this_tag_sent[-1][2][0] == 'V' :
			#Change the last word in question to aa ?
			ques = input_sents[i][:-2] + vowel_aa +'?'
			ans = avunu+', '+input_sents[i]
			yes_answers.append(ans)
			yes_questions.append(ques)
	#print(yes_questions)
	#print(yes_answers)
	return yes_questions,yes_answers

oka = 'ఒక'

def adjective_questions(tag_sents, input_sents):
	#For each sentence, find if a JJ exists, if JJ exists and is not "oka", replace it with elanti/etuvanti
	oka = 'ఒక'
	etuvanti = 'ఎలాంటి'
	adj_questions = []
	adj_answers = []
	#Single adjective questions
	for i in range(len(input_sents)):
		this_tag_sent = tag_sents[i]
		for j in range(len(this_tag_sent)):
			this_word = this_tag_sent[j]
			if this_word[2] == 'JJ' and this_word[0] != oka :
				index = j
				#replace jth word in input_sents with this. 
				adj_answers.append(input_sents[i])
				this_input_sent = input_sents[i].split(' ')
				if j!=0 and (this_input_sent[j-1] == oka or this_tag_sent[j-1][2] == 'DEM' ):
					this_input_sent[j-1] = ''
				this_input_sent[j] = etuvanti
				ques = ' '.join(this_input_sent) 
				ques = ques[:-1] + '?'
				adj_questions.append(ques)

	#tenses combination of adjective questions
			
	#print(adj_questions)
	#print(adj_answers)	
	return adj_questions, adj_answers

def quotative_questions(tag_sents, input_sents):
	emani = 'ఏమని'
	ani = 'అని' 
	open_quote = '“'
	close_quote='”'
	quot_qs = []
	quot_ans = []
	for i in range(len(tag_sents)):
		this_tag_sent = tag_sents[i]
		#Find if quotations exists in the sentence. 
		st = input_sents[i].find(open_quote)	
		en = input_sents[i].rfind(close_quote)
		en2 = input_sents[i].find(ani)
		if en!=-1:
			en2 = input_sents[i].find(ani, en)

		if st != -1 and en != -1 and en2 != -1:
			ques = input_sents[i][:st] + emani + input_sents[i][en2+len(ani):-1] + '?'
			quot_qs.append(ques)
			quot_ans.append(input_sents[i])

		this_input_sent = input_sents[i].split(' ')
	#Add more than 1 quotes in single sentence type questions
	#print(quot_qs)
	#print(quot_ans)
	return quot_qs, quot_ans

time_list = ['అప్పుడు', 'రోజు' , 'కాలం', 'సాయంకాలం', 'ఉదయం', 'మధ్యాహ్నం', 'రాత్రి', 'పగలు', 'నెల', 'వారం', 'సంవత్సరం', 'సూర్యాస్తమయం', 'శుభోదయం', 'దినం', 'సమయం', 'వర్తమానం' , 'పూర్వం', 'భవిష్యత్తు', 'సోమవారం', 'మంగళవారం', 'బుధవారం', 'గురువారం', 'శుక్రవారం', 'శనివారం', 'ఆదివారం', 'మాసం']

def ekkada_eppudu_questions(tag_sents, input_sents):
	v1_qs = []
	v1_ans = []
	lo = 'లో'
	ekkada = 'ఎక్కడ'
	oka = 'ఒక'
	eppudu = 'ఎప్పుడు'
	for i in range(len(input_sents)):
		st = []
		for j in range(len(tag_sents[i])):
			if tag_sents[i][j][2] == 'NN' and tag_sents[i][j][3] == lo:
				fl = 0
				if tag_sents[i][j][1] in set(time_list):
					fl=1
				st.append([j,fl])
		this_tag_sent = tag_sents[i]
			
		last_word = tag_sents[i][-1]
		if last_word[2][0] == 'V' and len(st) > 0:
			#Replace last verb with em + chey root + verb suffix
			#Find the word indexes which is noun and has suffix as lo
			for abcd in st:
				j, fl = abcd
				this_input_sent = input_sents[i].split(' ')
				if j!=0 and (this_input_sent[j-1] == oka or this_tag_sent[j-1][2] == 'DEM' or this_tag_sent[j-1][2][0] == 'Q'):
					this_input_sent[j-1] = ''
				if fl == 0:
					this_input_sent[j] = ekkada
				else:
					this_input_sent[j] = eppudu
				ques = ' '.join(this_input_sent)
				ques = ques[:-1]+'?'
				v1_ans.append(input_sents[i])
				v1_qs.append(ques)
	#print(v1_qs)
	#print(v1_ans)
	return v1_qs, v1_ans
#==========================
vowel_aa = b'\xe0\xb0\xbe'.decode('utf-8')
vowel_ae = b'\xe0\xb1\x87'.decode('utf-8')
vowel_i = b'\xe0\xb0\xbf'.decode('utf-8')
vowel_am = b'\xe0\xb0\x82'.decode('utf-8')
vowel_u = b'\xe0\xb1\x81'.decode('utf-8')
u = b'\xe0\xb9\x89'.decode('utf-8')
##############################
tundhi = vowel_u+vowel_am+'ది'
tunnadu = vowel_u+'న్నాడు'
tunnayi = vowel_u+'న్నాయి'
tunnaru = vowel_u+'న్నారు'
tunnavu = vowel_u+'న్నావు'
tunnanu = vowel_u+'న్నాను'
tunnamu = vowel_u+'న్నాము'
#-----------------------
chestundhi = 'చేస్తుంది'
chestunnadu = 'చేస్తున్నాడు'
chestunnayi = 'చేస్తున్నాయి'
chestunnaru = 'చేస్తున్నారు'
chestunnavu = 'చేస్తున్నావు'
chestunnanu = 'చేస్తున్నాను'
chestunnamu = 'చేస్తున్నాము'
#===========================
edhi = vowel_ae+'ది'
evi = vowel_ae+'వి'
evadu = vowel_ae + 'వాడు' 
evaru = vowel_ae + 'వారు'
evallu = vowel_ae + 'వాళ్ళు'
evadivi = vowel_ae + 'వాడివి'
edanivi = vowel_ae + 'దానివి'
evadini = vowel_ae + 'వాడిని'
edanini = vowel_ae + 'దానిని'
evallamu = vowel_ae + 'వాళ్ళము'
#----------
chesedhi = 'చేసేది'
chesevi = 'చేసేవి'
chesevadu = 'చేసేవాడు'
chesevaru = 'చేసేవారు'
chesevallu = 'చేసేవాళ్ళు'
chesevadivi = 'చేసేవాడివి'
chesedanivi = 'చేసేదానివి'
chesevadini = 'చేసేవాడిని'
chesedanini = 'చేసేదానిని'
chesevallamu = 'చేసేవాళ్ళము'
#================
indhi = vowel_i + vowel_am+ 'ది'
ayi = vowel_aa + 'యి'
adu = vowel_aa + 'డు'
aru= vowel_aa + 'రు'
avu= vowel_aa + 'వు'
anu= vowel_aa + 'ను'
amu= vowel_aa + 'ము'
ledhu = 'లేదు'
#----------
chesindhi = 'చేసింది' 
chesayi = 'చేసాయి'
chesadu = 'చేసాడు'
chesaru= 'చేసారు'
chesavu= 'చేసావు'
chesanu= 'చేశాను'
chesamu= 'చేసాము'
cheyaledhu = 'చేయలేదు'
#====================
untundhi = vowel_u+vowel_am + 'టుంది'
untayi = vowel_u + vowel_am + 'టాయి'
untaru = vowel_u + vowel_am + 'టారు'
chesthayi = 'చేస్తాయి'
chestharu = 'చేస్తారు'
##########
emi = 'ఏమి'
tenses = [ tundhi, tunnadu, tunnayi, tunnaru, tunnavu, tunnanu, tunnamu,
	 edhi, evi, evadu, evaru, evallu, evadivi, edanivi,evadini, edanini, evallamu,
	indhi, ayi,adu, aru, avu, anu, amu, ledhu, untundhi, untayi, untaru  ]
chestenses = [chestundhi, chestunnadu, chestunnayi, chestunnaru, chestunnavu, chestunnanu, chestunnamu,
	 chesedhi, chesevi, chesevadu, chesevaru, chesevallu, chesevadivi, chesedanivi,chesevadini, chesedanini, chesevallamu,
	 chesindhi, chesayi, chesadu, chesaru, chesavu, chesanu, chesamu, cheyaledhu, chestundhi, chesthayi, chestharu  ]

def tenses_verb_questions(tag_sents, input_sents):

	#Based on ending of the verb
	v2_qs = []
	v2_ans = []
	for i in range(len(input_sents)):
		#if last word ends with verb. 
		this_tag_sent = tag_sents[i]
		if this_tag_sent[-1][2][0]!='V':
			continue
		#tenses
		suffix = -1
		for j in range(len(tenses)):
			if this_tag_sent[-1][0].endswith(tenses[j]):
				suffix = j
				break

		if suffix >= 0:
			v2_ans.append(input_sents[i])
			this_input_sent = input_sents[i].split()
			#Remove last word, or the last 2 words if before is verb
			if len(this_tag_sent)>1 and this_tag_sent[-2][2][0] == 'V':
				this_input_sent[-2]=''
			this_input_sent[-1] = emi+ chestenses[suffix]
			ques = ' '.join(this_input_sent)
			ques = ques+'?'
			v2_qs.append(ques)	

	#print(v2_qs)
	#print(v2_ans)
	return v2_qs, v2_ans
def adverb_questions(tag_sents,input_sents):
	v12_qs = []
	v12_ans = []
	for i in range(len(tag_sents)):
		this_tag_sent = tag_sents[i]
		verb = this_tag_sent[-1]
		if verb[2][0] != 'V':
			continue
		suffix = -1
		for j in range(len(tenses)):
			if this_tag_sent[-1][0].endswith(tenses[j]):
				suffix = j
				
		allPOSes = [ word[2][0] for word in this_tag_sent]
		for j in range(len(this_tag_sent)-1):
			POStag = this_tag_sent[j][2]
			if POStag == 'RB':
				#Check if the first verb that comes after RB is the last word
				##print(allPOSes[j:-1])
				try:
					ind = len(allPOSes) -2- allPOSes[-2:j:-1].index('V') 
				except:
					ind = len(this_tag_sent)-1
				##print(input_sents[i])
				##print(ind)
				if ind==len(this_tag_sent)-1:
					
					v12_ans.append(input_sents[i])
					this_input_sent = input_sents[i].split()
					last_word = this_input_sent[-1]
					this_input_sent = this_input_sent[:j+1]
					if suffix >= len(chestenses):
						this_input_sent.append(emi+' '+last_word[:-1])
					else:
						this_input_sent.append(emi+chestenses[suffix])
					ques = ' '.join(this_input_sent) + '?'
					v12_qs.append(ques)
				else:
					v12_ans.append(input_sents[i])
					this_input_sent = input_sents[i].split()
					last_word = this_input_sent[-1]
					this_input_sent = this_input_sent[:ind+1]
					if suffix >= len(chestenses):
						this_input_sent.append(emi+' '+last_word[:-1])
					else:
						this_input_sent.append(emi+chestenses[suffix])
					ques = ' '.join(this_input_sent) + '?'
					v12_qs.append(ques)

	#print(v12_qs)
	#print(v12_ans)
	return v12_qs, v12_ans

yevaru = 'ఎవరు'
yedi = 'ఏది'
yevi = 'ఏవి'
di = 'ది'
ku = 'కు'
ki = 'కి'
tho = 'తో'
ni = 'ని'
nu = 'ను'
lo = 'లో'
suffs = [ki,ku,tho,ni,nu,lo]
deni  = 'దేని'
veti = 'వేటి'
yevari = 'ఎవరి'
yemi = 'ఏమి'
def subject_object_questions(tag_sents, input_sents, UD_sents):
	noun_qs = []
	noun_ans = []
	#You nkown what to replace, but, can you join it all together properly with punctuation?
	for i in range(len(input_sents)):
		this_UD = list(UD_sents[i]) #Remove all pads
		
		this_tag = tag_sents[i]
		tokenised = tokenize(input_sents[i][:-1])+['.']
		ind = len(tokenised)
		this_UD = this_UD[:ind]
		ind = -1

		if this_UD[-1] == 'punct' and this_UD[-2] == 'root':
			#Find the subject that is closest to root
			# print(this_UD)
			for j in range(len(tokenised)-2,-1,-1):
				# print(j)
				if this_UD[j]== 'nsubj' :
					if (this_tag[j][2]=='NN' or this_tag[j][2] =='PRP') :
						#print(tokenised[j],this_tag[j])
						ind = j
						break
			if ind == -1:
				continue
			if this_tag[ind][5] == 'sg':
				if this_tag[-1][0].endswith(di) or this_tag[-1][0].endswith(ledhu):
					tokenised[ind] = yedi
					for m in range(len(suffs)):
						if this_tag[ind][0].endswith(suffs[m]):
							tokenised[ind] =  deni+suffs[m]
				elif this_tag[-1][0].endswith(aru):
					tokenised[ind]=yevaru
					for m in range(len(suffs)):
						if this_tag[ind][0].endswith(suffs[m]):
							tokenised[ind] =  yevari+suffs[m]
				else:
					tokenised[ind] = yemi
					for m in range(len(suffs)):
						if this_tag[ind][0].endswith(suffs[m]):
							tokenised[ind] =  deni+suffs[m]

			elif this_tag[ind][5] =='pl':
				if this_tag[-1][0].endswith(ayi) or this_tag[-1][0].endswith(ledhu):
					tokenised[ind] = yevi
					for m in range(len(suffs)):
						if this_tag[ind][0].endswith(suffs[m]):
							tokenised[ind] =  veti+suffs[m]

				else:
					tokenised[ind]=yevaru
					for m in range(len(suffs)):
						if this_tag[ind][0].endswith(suffs[m]):
							tokenised[ind] =  yevari+suffs[m]

			else:
				continue
			if ind!=0 and (this_tag[ind-1][2]=='DEM' or this_tag[ind-1][0] == oka):
				tokenised[ind-1] ='' 
			ques = ' '.join(tokenised[:-1])+'?'
			noun_qs.append(ques)
			noun_ans.append(input_sents[i])
	# print(noun_qs)
	# print(noun_ans)
	return noun_qs, noun_ans			

if __name__ == '__main__':

	#Preprocess input file and get the sentences
	input_filename = './telugu.input.txt'
	f = open(input_filename,'r')
	input_contents = f.read()

	#Input sentences has normal split sentences
	#Input filtered sentences - same but without the punctuation
	input_sentences, input_filtered_sentences = clean_text(input_contents)
	f.close()

	#Preprocess output file and get the tags
	tag_file = './telugu.output.txt'
	tag_sentences = tagfile_preprocess(tag_file, input_filtered_sentences, input_sentences)

	#UD tags
	UD_sentences = np.load('UD_tags.npy')
	noun_questions, noun_answers = subject_object_questions(tag_sentences, input_sentences, UD_sentences)

	#Generate YES sentences; add "aa" to the end
	yes_questions, yes_answers = positive_questions(tag_sentences, input_sentences)
	
	#Generate elanti sentences; 
	adj_questions, adj_answers = adjective_questions(tag_sentences, input_sentences)
	#Generate quotative questions
	quot_questions, quot_answers = quotative_questions(tag_sentences, input_sentences)
	
	#Generate ekkada/eppudu questions
	v1_questions, v1_answers = ekkada_eppudu_questions(tag_sentences, input_sentences)

	#Generate action questions
	v11_questions, v11_answers = adverb_questions(tag_sentences,input_sentences)
	# v2_questions, v2_answers = tenses_verb_questions(tag_sentences, input_sentences)

	#Combine all question answer pairs nicely and write to file	
	all_questions = noun_questions + yes_questions + adj_questions + quot_questions + v1_questions + v11_questions
	all_answers = noun_answers + yes_answers + adj_answers + quot_answers + v1_answers + v11_answers

	print(" ========SUMMARY===========")
	for i in range(len(input_sentences)):
		print(input_sentences[i])
	print()
	print(" ========QA Pairs =========")
	for i in range(len(all_questions)):
		print('Q' +': '+all_questions[i])
		print('A'+': '+all_answers[i])
		print()
