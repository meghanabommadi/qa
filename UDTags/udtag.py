import re
import io
import string
import operator
from collections import OrderedDict
import sys
import numpy as np
import freq
import json

train_s = open("telugu_train_sent.txt", 'r')
trainS = train_s.read()
train_t = open("telugu_train_tag.txt", 'r')
trainT = train_t.read()
test_s = open("telugu_test_sent.txt", 'r')
testS = test_s.read()
test_t = open("telugu_test_tag.txt", 'r')
testT = test_t.read()

# output = open(sys.argv[2], 'w+')


# ok = open(sys.argv[1],'r')
# note = ok.read()
tokens = []
for x in range(len(freq.se1)):
    tokens.append(freq.tokenize(freq.se1[x]))

def tokenize(book):
    if book is not None:
        
        words = book.split()
        return words
    else:
        return None
def sentences(book):
    if book is not None:
        # senten = book.split("\n")
        senten = re.split("\n\n" , book)
        # senten = senten.split(".")
        # senten = book.split("\.\s")
        # print(senten)
    return senten
# words = tokenize()
trainse = sentences(trainS)
testse = sentences(testS)
trainta = sentences(trainT)
testta = sentences(testT)
# no = len(se)
words = []


train_sentences = {}
test_sentences = {}
train_tags = {}
test_tags = {}


for x in range(len(trainse)):
    words = tokenize(trainse[x])
    train_sentences[x] = words
for x in range(len(testse)):
    words = tokenize(testse[x])
    test_sentences[x] = words
for x in range(len(trainta)):
    words = tokenize(trainta[x])
    train_tags[x] = words
for x in range(len(testta)):
    words = tokenize(testta[x])
    test_tags[x] = words

# print(test_tags[5])
# print(test_sentences[5])

# print(len(test_tags))
# print(len(test_sentences))



words, tags = set([]), set([])

for s in train_sentences:
    for w in train_sentences[s]:
        words.add(w.lower())

for ts in train_tags:
    for t in train_tags[ts]:
        tags.add(t)


word2index = {w: i + 2 for i, w in enumerate(list(words))}
word2index['-PAD-'] = 0  
word2index['-OOV-'] = 1  
tag2index = {t: i + 1 for i, t in enumerate(list(tags))}
tag2index['-PAD-'] = 0 
tag2index['-OOV-'] = 1  

train_sentences_X, test_sentences_X, train_tags_y, test_tags_y = [], [], [], []

for s in train_sentences:
    s_int = []
    for w in train_sentences[s]:
        try:
            s_int.append(word2index[w.lower()])
        except KeyError:
            s_int.append(word2index['-OOV-'])

    train_sentences_X.append(s_int)

for s in test_sentences:
    s_int = []
    for w in test_sentences[s]:
        try:
            s_int.append(word2index[w.lower()])
        except KeyError:
            s_int.append(word2index['-OOV-'])
    test_sentences_X.append(s_int)


# for s in train_tags:
#     s_int = []
#     for w in train_tags[s]:
#         try:
#             s_int.append(tag2index[w.lower()])
#         except KeyError:
#             s_int.append(tag2index['-OOV-'])

#     train_tags_y.append(s_int)

# for s in test_tags:
#     s_int = []
#     for w in test_tags[s]:
#         try:
#             s_int.append(tag2index[w.lower()])
#         except KeyError:
#             s_int.append(tag2index['-OOV-'])

#     test_tags_y.append(s_int)

for s in train_tags:
    train_tags_y.append([tag2index[t] for t in train_tags[s]])

for s in test_tags:
    test_tags_y.append([tag2index[t] for t in test_tags[s]])

print(len(test_sentences_X))
print(len(test_tags_y))

print(train_sentences_X[5])
print(test_sentences_X[5])
print(train_tags_y[5])
print(test_tags_y[5])



MAX_LENGTH = len(max(train_sentences_X, key=len))
print(MAX_LENGTH)  

from keras.preprocessing.sequence import pad_sequences

train_sentences_X = pad_sequences(train_sentences_X, maxlen=MAX_LENGTH, padding='post')
test_sentences_X = pad_sequences(test_sentences_X, maxlen=MAX_LENGTH, padding='post')
train_tags_y = pad_sequences(train_tags_y, maxlen=MAX_LENGTH, padding='post')
test_tags_y = pad_sequences(test_tags_y, maxlen=MAX_LENGTH, padding='post')

print(train_sentences_X[0])
print(test_sentences_X[0])
print(train_tags_y[0])
print(test_tags_y[0])



from keras.models import Sequential
from keras.layers import Dense, LSTM, InputLayer, Bidirectional, TimeDistributed, Embedding, Activation
from keras.optimizers import Adam


model = Sequential()
model.add(InputLayer(input_shape=(MAX_LENGTH, )))
model.add(Embedding(len(word2index), 128))
model.add(Bidirectional(LSTM(256, return_sequences=True)))
model.add(TimeDistributed(Dense(len(tag2index))))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer=Adam(0.001),
              metrics=['accuracy'])

model.summary()


def to_categorical(sequences, categories):
    cat_sequences = []
    for s in sequences:
        cats = []
        for item in s:
            cats.append(np.zeros(categories))
            cats[-1][item] = 1.0
        cat_sequences.append(cats)
    return np.array(cat_sequences)

cat_train_tags_y = to_categorical(train_tags_y, len(tag2index))
print(cat_train_tags_y[0])
print(len(test_sentences_X))
print(len(test_tags_y))
print(len(train_tags_y))
print(len(train_sentences_X))
model.fit(train_sentences_X, to_categorical(train_tags_y, len(tag2index)), batch_size=128, epochs=120, validation_split=0.2)

scores = model.evaluate(test_sentences_X, to_categorical(test_tags_y, len(tag2index)))
print({scores[1] * 100})


# test_samples = [
#     "మనం ఎందుకు అన్నం తింటాం ?".split(),
#     "నువ్వు పని చెయ్యాలి .".split(),
#     "నేను ఆ పని చెయ్యాల్సి వచ్చింది .".split(),
#     "మీ తల్లిదండ్రుల ఊరు ఏమిటి ?".split(),
#     "అవి మా ఇళ్ళ గోడలు .".split(),
#     "ఒకానొకప్పుడు ఒక ఊరిలో ఒక అమాయకపు పిచుక వుండేది".split(),
#     "ఆ పిచుక తన ఇంటికి వెళ్తుంది".split(),
#     'మనసులో ఏ కల్మషంలేని ఆ పిచుకకి ఒక రోజు ఒక కాకులగుంపు పరిచయం అయ్యింది.'.split(),
#     ' ఆ కాకులతో పిచుకకి స్నేహం అయ్యింది.'.split()
# ]

print("-----------; TOKENS COME HERE; -------")
print(freq.se1)
print("-------------------")
test_samples = tokens
for x in range(len(test_samples)):
    test_samples[x].append(".")
# samplesset = sentences(note)

# test_samples = {}
# for x in range(len(samplesset)):
#     test_samples[x] = tokenize(samplesset[x])
# print(test_samples)
# print(test_samples)
with open('saved_word2index.json','w') as fp:
	json.dump(word2index,fp) 
  
with open('saved_tag2index.json','w') as fp:
	json.dump(tag2index,fp) 

test_samples_X = []
for s in test_samples:
    s_int = []
    for w in s:
        try:
            s_int.append(word2index[w.lower()])
        except KeyError:
            s_int.append(word2index['-OOV-'])
    test_samples_X.append(s_int)

test_samples_X = pad_sequences(test_samples_X, maxlen=MAX_LENGTH, padding='post')
print("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS")
print('MAX LENGTH =',MAX_LENGTH)
print(test_samples_X)

predictions = model.predict(test_samples_X)
print(predictions, predictions.shape)
print('TTTTTTTTTTTTTTTTTTTTTTTTTTTT')
def logits_to_tokens(sequences, index):
    token_sequences = []
    for categorical_sequence in sequences:
        token_sequence = []
        for categorical in categorical_sequence:
            token_sequence.append(index[np.argmax(categorical)])

        token_sequences.append(token_sequence)

    return token_sequences

newtokens = logits_to_tokens(predictions, {i: t for t, i in tag2index.items()})
print(newtokens)
print("THINGGGGGGGGGGGGGGGGGGGGGGGG")


from keras import backend as K

def ignore_class_accuracy(to_ignore=0):
    def ignore_accuracy(y_true, y_pred):
        y_true_class = K.argmax(y_true, axis=-1)
        y_pred_class = K.argmax(y_pred, axis=-1)

        ignore_mask = K.cast(K.not_equal(y_pred_class, to_ignore), 'int32')
        matches = K.cast(K.equal(y_true_class, y_pred_class), 'int32') * ignore_mask
        accuracy = K.sum(matches) / K.maximum(K.sum(ignore_mask), 1)
        return accuracy
    return ignore_accuracy

from keras.models import Sequential
from keras.layers import Dense, LSTM, InputLayer, Bidirectional, TimeDistributed, Embedding, Activation
from keras.optimizers import Adam


model = Sequential()
model.add(InputLayer(input_shape=(MAX_LENGTH, )))
model.add(Embedding(len(word2index), 128))
model.add(Bidirectional(LSTM(256, return_sequences=True)))
model.add(TimeDistributed(Dense(len(tag2index))))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer=Adam(0.001),
              metrics=['accuracy', ignore_class_accuracy(0)])

model.summary()


model.fit(train_sentences_X, to_categorical(train_tags_y, len(tag2index)), batch_size=128, epochs=120, validation_split=0.2)


predictions = model.predict(test_samples_X)


newtoken2 = (logits_to_tokens(predictions, {i: t for t, i in tag2index.items()}))
print(newtoken2)
print(test_samples)

scores = model.evaluate(test_sentences_X, to_categorical(test_tags_y, len(tag2index)))
print({scores[1] * 100})

# tag2index have the outputs we can use for question formation...............................................................

# output.write(print(logits_to_tokens(predictions, {i: t for t, i in tag2index.items()})))
# model.save()

# model1 = create_model()
# model1.fit(train_sentences_X, to_categorical(train_tags_y, len(tag2index)), batch_size=128, epochs=120, validation_split=0.2)
model_json = model.to_json()
with open("saved_model.json","w") as json_file:
	json_file.write(model_json)
model.save_weights('saved_model.h5')
print("Saved model to disk")
# from keras.models import load_model
# new_model = load_model('saved_model.h5')
# new_model.summary()
