import re
import io
import string
import operator
from collections import OrderedDict
import sys

file = open(sys.argv[1], 'r')
book = file.read()

file2 = open('stopwords.txt', 'r')
swords = file2.read()

file3  = open(sys.argv[2], 'r')
lamp = file3.read()

# print("Original text:")
# print(book)
# print("..............................................................................................")
def swlist():
    if swords is not None:
        words = swords.replace(', ',' ')
        words = swords.replace(',',' ')
        words = swords.replace('. ',' ')
        words = swords.replace('.', ' ' )
        words = swords.split()
        return words
    else:
        return None
stop_words = swlist()

def tokenize(bookl):
    if bookl is not None:
        words = bookl.replace(', ',' ')
        words = bookl.replace(',',' ')
        words = bookl.replace('. ',' ')
        words = bookl.replace('.', ' ' )
        words = bookl.replace('"', '" ' )
        words = bookl.replace('', '\s"\s' )
        # print(book)
        words = bookl.split()
        return words
    else:
        return None
def sentences(bookl):
    if bookl is not None:
        # senten = book.split("\n")
        senten = re.split("\.|\n|\?" , bookl)
        # senten = senten.split(".")
        # senten = book.split("\.\s")
        # print(senten)
    return senten

def count_word(tokens, token):
    count = 0

    for element in tokens:
        # Remove Punctuation
        word = element.replace(",","")
        word = word.replace(".","")

        # Found Word?
        if word == token:
            count += 1
    return count

def getUniqueWords(allWords) :
    uniqueWords = [] 
    for i in allWords:
        if not i in uniqueWords:
            uniqueWords.append(i)
    return uniqueWords    
    
def map_book(tokens):
    hash_map = {}

    if tokens is not None:
        for element in tokens:
            # Remove Punctuation
            word = element.replace(", "," ")
            word = word.replace(". "," ")
     
            if word in hash_map:
                hash_map[word] = hash_map[word] + 1
            else:
                hash_map[word] = 1

        return hash_map
    else:
        return None

words = tokenize(lamp)
fwords = []
for x in range(len(words)): #words without stop words
    if words[x] not in stop_words:
        fwords.append(words[x])


word_list = getUniqueWords(fwords)
map = map_book(fwords)

freq = []
for word in word_list:
    freq.append(map[word])


k = 0
s = 'ok'
my_dict = {}
my_dict["w"]= word_list
my_dict["k"] = freq

dic = OrderedDict()


i=0

for key in freq:
    dic.setdefault(key, []).append(word_list[i])
    i += 1
sorted_x = sorted(dic.items(), key=operator.itemgetter(0))

se1 = sentences(book)
se = sentences(lamp)

high_w = []
i = (len(dic) - 1)
for i in reversed(range(len(dic))):
    high_w.append(sorted_x[i][0])
    i = i-1


k = len(high_w)//2
final = []
for i in range(0,len(se)):
    for word in word_list:
        if map[word] == high_w[0] or map[word] == high_w[1]or map[word] == high_w[2] or map[word] == high_w[3]:
            if word in se[i]:
                if se1[i] not in final:
                    final.append(se1[i])  
# print("summary of the text:")

# print(final)


# print("no. of sentences in summary:")
# print(len(final))
# print("no. of sentences in original text:")
# print(len(se))
# print(len(se1))
