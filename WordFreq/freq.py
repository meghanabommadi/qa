import re
import io
import string
import operator
from collections import OrderedDict
import sys
# import stopwords.txt
# from nltk.corpus import stopwords
# nltk.download('stopwords')
# from nltk.tokenize import word_tokenize

file = open(sys.argv[1], 'r')
book = file.read()

file2 = open('stopwords.txt', 'r')
swords = file2.read()
print("Original text:")
print(book)
print("..............................................................................................")
def swlist():
    if swords is not None:
        # words = re.sub('(\.\s)',r' \1',book)
        # words = re.sub('(\,\s)',r' \1',book)
        words = swords.replace(', ',' ')
        words = swords.replace(',',' ')
        words = swords.replace('. ',' ')
        words = swords.replace('.', ' ' )
        # print(book)
        words = swords.split()
        return words
    else:
        return None
stop_words = swlist()

def tokenize():
    if book is not None:
        # words = re.sub('(\.\s)',r' \1',book)
        # words = re.sub('(\,\s)',r' \1',book)
        words = book.replace(', ',' ')
        words = book.replace(',',' ')
        words = book.replace('. ',' ')
        words = book.replace('.', ' ' )
        # print(book)
        words = book.split()
        return words
    else:
        return None
def sentences():
    if book is not None:
        # senten = book.split("\n")
        senten = re.split("\.|\n" , book)
        # senten = senten.split(".")
        # senten = book.split("\.\s")
        # print(senten)
    return senten

def count_word(tokens, token):
    count = 0

    for element in tokens:
        # Remove Punctuation
        word = element.replace(",","")
        word = word.replace(".","")

        # Found Word?
        if word == token:
            count += 1
    return count

def getUniqueWords(allWords) :
    uniqueWords = [] 
    for i in allWords:
        if not i in uniqueWords:
            uniqueWords.append(i)
    return uniqueWords    
    
def map_book(tokens):
    hash_map = {}

    if tokens is not None:
        for element in tokens:
            # Remove Punctuation
            word = element.replace(", "," ")
            word = word.replace(". "," ")
     
            if word in hash_map:
                hash_map[word] = hash_map[word] + 1
            else:
                hash_map[word] = 1

        return hash_map
    else:
        return None

words = tokenize()
fwords = []

for x in range(len(words)): #words without stop words
    if words[x] not in stop_words:
        # print("ohuhuuhuu")
        fwords.append(words[x])

# print(fwords)
# print(words)

word_list = getUniqueWords(fwords)
map = map_book(fwords)

freq = []
for word in word_list:
    freq.append(map[word])


k = 0
s = 'ok'
my_dict = {}
# for word in word_list:
#     # for the highest frequency
#     if map[word] > k:
#         s=word
#         k = map[word]


my_dict["w"]= word_list
my_dict["k"] = freq

dic = OrderedDict()


i=0

for key in freq:
    dic.setdefault(key, []).append(word_list[i])
    i += 1
sorted_x = sorted(dic.items(), key=operator.itemgetter(0))

se = sentences()


high_w = []
i = (len(dic) - 1)
for i in reversed(range(len(dic))):
    # print(i)
    high_w.append(sorted_x[i][0])
    i = i-1


k = len(high_w)//2
final = []
for i in range(0,len(se)):
    for word in word_list:
        if map[word] == high_w[0] or map[word] == high_w[1] :
            # print(high_w[0]) 
            # print(word)
            if word in se[i]:
                # [temp.append(x) for x in ints_list if x not in temp]
                if se[i] not in final:
                    final.append(se[i])  
                # print(se[i])
print("summary of the text:")

print(final)


print("no. of sentences in summary:")
print(len(final))
print("no. of sentences in original text:")
print(len(se))
